/* KIOSKWM.C
 * PUBLIC DOMAIN
 * Based on MINWM.C (https://bbs.archlinux.org/viewtopic.php?pid=1676756#p1676756)
 * Compile: cc -o kioskwm kioskwm.c -lX11
 */

#include <X11/Xlib.h>
#include <X11/keysym.h>

int main(int argc, const char **argv) {
	Display *dpy;
	Window root;
	int sw, sh;
	XEvent xev;
	XMapRequestEvent *xmrev;

	dpy = XOpenDisplay(0x0);
	if (dpy == NULL) {
		return 1;
	}
	root = DefaultRootWindow(dpy);
	sw = DisplayWidth(dpy, DefaultScreen(dpy));
	sh = DisplayHeight(dpy, DefaultScreen(dpy));
	XSelectInput(dpy, root, SubstructureRedirectMask);
	while (!XNextEvent(dpy, &xev)) {
		switch (xev.type) {
		case MapRequest:
			xmrev = &xev.xmaprequest;
			XMoveResizeWindow(dpy, xmrev->window, 0, 0, sw, sh);
			XMapWindow(dpy, xmrev->window);
			break;
		}
	}
	return 0;
}
